use crate::apis;

#[derive(Debug, Clone)]
pub struct ScriptEvent {
    pub event_name: String,
    pub bot: apis::Bot,
    pub channel: Option<apis::Channel>,
    pub message: Option<apis::Message>
    //pub context: serenity::client::Context,
    //pub message: serenity::model::channel::Message
}

impl ScriptEvent {
    pub fn new(event_name: String, _context: &serenity::client::Context, message: &serenity::model::channel::Message) -> Self {
        Self {
            event_name,
            bot: apis::Bot::new(),
            channel: Some(apis::Channel {
                name: String::from("#botfax")
            }),
            message: Some(apis::Message {
                body: message.content.clone()
            })
        }
    }
}
