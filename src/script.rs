use crate::apis::{
    Bot,
    bot_module,
    Channel,
    channel_module,
    Message as BogusMessage,
    message_module
};
use crate::script_event::ScriptEvent;

use rhai::packages::Package;
use rhai_rand::RandomPackage;
use std::path::PathBuf;

pub struct Script {
    // Scripting engine
    pub engine: rhai::Engine,
    // Use a custom 'Scope' to keep stored state
    pub scope: rhai::Scope<'static>,
    // Program script
    pub ast: rhai::AST
}

impl Script {
    pub fn new(path: PathBuf) -> Self {
        let mut engine = rhai::Engine::new();
        // Expressions nesting at the global level
        // and inside functions, respectively
        engine.set_max_expr_depths(64, 32);

        engine.register_global_module(RandomPackage::new().as_shared_module());
        // Register custom types and APIs
        engine.register_type_with_name::<Bot>("Bot")
              .register_global_module(exported_module!(bot_module).into());
        engine.register_type_with_name::<Channel>("Channel")
              .register_global_module(exported_module!(channel_module).into());
        engine.register_type_with_name::<BogusMessage>("Message")
              .register_global_module(exported_module!(message_module).into());

        // Create a custom 'Scope' to hold state
        let mut scope = rhai::Scope::new();

        // Compile the handler script.
        // In a real application you'd be handling errors...
        let ast = engine.compile_file(path).unwrap();

        // Evaluate the script to initialize it and other state variables.
        // In a real application you'd again be handling errors...
        engine.run_ast_with_scope(&mut scope, &ast).unwrap();

        // The event handler is essentially these three items:
        Self { engine, scope, ast }
    }

    // Trigger a callback in Rhai
    pub fn trigger(&mut self, payload: ScriptEvent) {
        let engine = &self.engine;
        let ast = &self.ast;

        match payload.event_name.as_ref() {
            "setup" => {
                engine.call_fn(&mut self.scope, &ast, "setup", (payload.bot,))
                    .or_else(|err| match *err {
                        rhai::EvalAltResult::ErrorFunctionNotFound(fn_name, _) if fn_name.starts_with("setup") => {
                            // Turn function-not-found into a success
                            Ok(rhai::Dynamic::UNIT)
                        }
                        _ => Err(err)
                    }).unwrap();
            },
            "on_connect" => {
                engine.call_fn(&mut self.scope, &ast, "on_connect", (payload.bot, payload.channel.unwrap()))
                    .or_else(|err| match *err {
                        rhai::EvalAltResult::ErrorFunctionNotFound(fn_name, _) if fn_name.starts_with("on_message") => {
                            // Turn function-not-found into a success
                            Ok(rhai::Dynamic::UNIT)
                        }
                        _ => Err(err)
                    }).unwrap();
            },
            "on_message" => {
                engine.call_fn(&mut self.scope, &ast, payload.event_name, (payload.bot, payload.channel.unwrap(), payload.message.unwrap()))
                    .or_else(|err| match *err {
                        rhai::EvalAltResult::ErrorFunctionNotFound(fn_name, _) if fn_name.starts_with("on_message") => {
                            // Turn function-not-found into a success
                            Ok(rhai::Dynamic::UNIT)
                        }
                        _ => Err(err)
                    }).unwrap();
            },
            m => {
                println!("invalid callback trigger: \"{}\"", m);
            }
        }
    }
}
