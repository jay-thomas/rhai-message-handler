use rhai::plugin::*;

/// The channel context object map
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct Channel {
    pub name: String
}

#[export_module]
pub mod channel_module {
    #[rhai_fn(global)]
    pub fn say(_channel: &mut Channel, text: &str) {
        println!("[Channel.Say] \"{}\"", text)
    }
}

