use rhai::plugin::*;

/// The bot object map
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct Bot {
    pub name: String
    //token: String
}

impl Bot {
    pub fn new() -> Bot {
        Bot {
            name: String::from("Dark")
            //token: cfg.token.clone()
        }
    }
}

#[export_module]
pub mod bot_module {
    #[rhai_fn(global)]
    pub fn log(_bot: &mut Bot, text: &str) {
        println!("[Info] \"{}\"", text);
    }
}
