//use crate::config::Config;
use rhai::plugin::*;

/// The message context object map
#[derive(Debug, Clone, Eq, PartialEq, Hash, Default)]
pub struct Message {
    pub body: String
}

/*
impl Message {
    pub fn new(config: Config, message: String) -> Message {
        Message {
            body: message
        }
    }
}
*/

#[export_module]
pub mod message_module {
    #[rhai_fn(global)]
    pub fn reply(_message: &mut Message, text: &str) {
        println!("[Message.Reply] \"{}\"", text)
    }

    #[rhai_fn(global)]
    pub fn func1(message: &mut Message) -> bool {
        println!("In func1. Here's the data: {}", message.body);
        false
    }

    pub fn process(data: i64) -> i64 {
        println!("Processing... {}", data);
        300
    }

    // Getter
    #[rhai_fn(get = "body", pure)]
    pub fn get_value(message: &mut Message) -> String {
        message.body.clone()
    }

    // Setter
    #[rhai_fn(set = "body")]
    pub fn set_value(message: &mut Message, value: String) {
        message.body = value;
    }
}
