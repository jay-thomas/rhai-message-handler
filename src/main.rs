#[macro_use]
extern crate rhai;
extern crate rhai_rand;

use dotenv::dotenv;
use std::process;
use std::path::PathBuf;
use std::fs::read_dir;
use tokio::sync::broadcast;

use serenity::{
    //model::{channel::Message, gateway::Ready},
    prelude::*,
};


mod apis;
mod config;
mod handler;
mod script;
mod script_event;

#[tokio::main]
async fn main() {
    dotenv().ok();
    let cfg = match config::Config::new() {
        Ok(c) => c,
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        }
    };

    let buffer_size: usize = 32;
    let script_paths = find_scripts(&cfg.directory);
    // Channel: Script -> Master
    //let (tx_script, rx_master) = mpsc::channel(100);
    // Channel: Master -> Script
    type MasterChannel = (broadcast::Sender<script_event::ScriptEvent>, broadcast::Receiver<script_event::ScriptEvent>);
    let (tx_master, _rx_script): MasterChannel = broadcast::channel(buffer_size);

    for script_path in script_paths.iter() {
        let mut rx_script = tx_master.subscribe();
        let mut script = script::Script::new(script_path.clone());
        tokio::spawn(async move {
            loop {
                let payload = rx_script.recv().await.unwrap();
                script.trigger(payload);
            }
        });
    }

    let mut client = Client::builder(&cfg.token)
        .event_handler(handler::Handler { tx_master })
        .await
        .expect("Error creating Discord client");

    // Client instances will automatically attempt to reconnect,
    // and will perform exponential backoff until it reconnects.
    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}

fn find_scripts<'a>(directory: &'a PathBuf) -> Vec<PathBuf> {
    let paths = read_dir(directory).expect("Could not open the given directory");
    let mut scripts = Vec::new();
    for path in paths {
        let path = path.unwrap().path();
        println!("Name: {}", path.display());
        if let Some(ext) = path.extension() {
            if ext == "rhai" {
                println!("Loading script \"{}\"", path.display());
                scripts.push(path);
            }
        }
    }
    scripts
}
