use std::env;
use std::path::PathBuf;
use structopt::StructOpt;

const NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = concat!(env!("CARGO_PKG_VERSION"), " (", env!("GIT_HASH"), ")");
const AUTHORS: &str = env!("CARGO_PKG_AUTHORS");

/// Rhai Discord bot main executable
#[derive(StructOpt, Debug)]
#[structopt(name=NAME, version = VERSION, author = AUTHORS)]
pub struct Args {
    /// Discord's API app ID (overrides environment variable)
    #[structopt(long)]
    pub token: Option<String>,
    /// Directory to load Rhai scripts from
    #[structopt(long)]
    pub directory: Option<String>,
    /// Command prefixes to respond to, comma separated
    #[structopt(long)]
    pub prefixes: Option<String>,
}

pub struct Config {
    pub prefixes: Vec<String>,
    pub directory: PathBuf,
    pub token: String
}

impl Config {
    pub fn new() -> Result<Config, String> {
        let args = Args::from_args();

        let directory = args.directory.or_else(|| env::var("DIRECTORY").ok());
        let directory = match directory {
            Some(d) => PathBuf::from(d),
            None => PathBuf::from("./scripts")
        };

        let prefixes = args.prefixes.unwrap_or_else(
            || env::var("PREFIXES").unwrap_or(String::from("."))
        );

        let token = args.token.or_else(|| env::var("TOKEN").ok());
        let token = match token {
            Some(t) => t,
            None => return Err("A Discord token is required.".into())
        };
        println!("{}", token);

        Ok(Config {
            directory,
            prefixes: vec![prefixes],
            token
        })
    }
}
