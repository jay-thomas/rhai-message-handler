use crate::script_event::ScriptEvent;

use serenity::{
    async_trait,
    model::{channel::Message, gateway::Ready},
    prelude::*
};

pub struct Handler {
    pub tx_master: tokio::sync::broadcast::Sender<ScriptEvent>
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, context: Context, message: Message) {
        self.tx_master.send(ScriptEvent::new(String::from("on_message"), &context, &message)).unwrap();
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}
